import { Component, OnInit, Input } from '@angular/core';
import { IJournalEntry } from '../lib/model';

@Component({
  selector: 'app-journal-list',
  templateUrl: './journal-list.component.html',
  styleUrls: ['./journal-list.component.css']
})
export class JournalListComponent implements OnInit {

  @Input() journalEntries: IJournalEntry[];
  currentJournalEntry: IJournalEntry = null;
  journalStatus: string = 'private';

  constructor() { }

  ngOnInit() {
  }

  clickJournalEntry(entry: IJournalEntry) {
    this.currentJournalEntry = entry;
    if (this.currentJournalEntry.status == 0) {
      this.journalStatus = 'Private';
    } else if (this.currentJournalEntry.status == 1) {
      this.journalStatus = 'Friends Only';
    } else {
      this.journalStatus = 'Public';
    }
    console.log('clicked journalEntry');
  }

  onClose() {
    this.currentJournalEntry = null;
  }
}
