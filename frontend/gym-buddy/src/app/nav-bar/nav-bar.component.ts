import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';
import { ProfileService } from '../services/profile.service';
import { BehaviorSubject, interval, timer } from 'rxjs';
import { IFriendRequest } from '../lib/model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  friends: string[] = [];
  values = '';

  constructor(private accountService: AccountService,
              private profileService: ProfileService,
              private router: Router) { }

  onKey(event: any) { // without type info
    this.values = event.target.value;
  }
  ngOnInit() {
    /*
    this.getFriendRequests();
    interval(10000).subscribe(val => {
      this.getFriendRequests();
    });
    */
  }

  getFriendRequests() {
    this.profileService.getFriendRequests().subscribe(
      result => {
        console.log("requests", result);
        this.friends = result;
      },
      err => {
      }
    );
  }

  acceptFriendRequest(username: string) {
    this.profileService.acceptFriendRequest(username).subscribe(
      result => {
      },
      err => {
      }
    );
    for (let i = 0; i < this.friends.length; i++) {
      if (this.friends[i] == username) {
        this.friends.splice(i, 1);
        break;
      }
    }
  }

  rejectFriendRequest(username: string) {
    this.profileService.rejectFriendRequest(username).subscribe(
      result => {
      },
      err => {
      }
    );
    for (let i = 0; i < this.friends.length; i++) {
      if (this.friends[i] == username) {
        this.friends.splice(i, 1);
        break;
      }
    }
  }

  logout() {
    localStorage.removeItem("email");
    this.accountService.loggedIn$.next(false);
  }

  onUserSearch() {
    console.log('hi');
    let query = this.values;
    this.values = "";
    this.router.navigate(['/results'], { queryParams: {
      type: 'users',
      search: query
    }});
  }

  onTagSearch() {
    console.log('yo');
    let query = this.values;
    this.values = "";
    this.router.navigate(['/results'], { queryParams: {
      type: 'tags',
      search: query
    }});
  }

}
