export interface IProfile {
  name: string;
  userName: string;
  age: number;
  height: number;
  weight: number;
  bmi: number;
  bio: string;
  status: number;
}

export interface IAccount {
  name: string;
  email: string;
  password1: string;
  password2: string;
}

export interface IUser {
  email: string;
  password: string;
}

export interface IWorkout {
  wName: string;
  date: string;
  email: string;
  url: string;
  status: number;
  exercises: any;
  tags: string[];
}

export interface IJournalEntry {
  dName: string;
  date: string;
  email: string;
  entry: string;
  status: number;
}

export interface IFriendRequest {
  username: string;
}

export interface IPost {
  pName: string;
  date: string;
  email: string;
  entry: string;
  status: string;
  workout: IWorkout[];
  diet: IJournalEntry[];
}

export interface IResults {
  results: IAccount[];
}
