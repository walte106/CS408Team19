import { Component, OnInit, Input } from '@angular/core';
import { IWorkout } from '../lib/model';

@Component({
  selector: 'app-workout-list',
  templateUrl: './workout-list.component.html',
  styleUrls: ['./workout-list.component.css']
})
export class WorkoutListComponent implements OnInit {

  @Input() workouts: IWorkout[];
  currentWorkout: IWorkout = null;
  workoutStatus: string = 'private';
  arrayOfKeys;

  constructor() { }

  ngOnInit() {
  }

  clickWorkout(workout: IWorkout) {
    this.currentWorkout = workout;
    if (this.currentWorkout.status == 0) {
      this.workoutStatus = 'Private';
    } else if (this.currentWorkout.status == 1) {
      this.workoutStatus = 'Friends Only';
    } else {
      this.workoutStatus = 'Public';
    }
    this.currentWorkout = workout;
    this.arrayOfKeys = Object.keys(this.currentWorkout.exercises);
    console.log('clicked workout: ', workout);
  }

  onClose() {
    this.currentWorkout = null;
    this.arrayOfKeys = [];
  }

}
