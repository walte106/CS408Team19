import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Common Angular modules.
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClarityModule, ClrFormsNextModule } from '@clr/angular';

import { AppComponent } from './app.component';
import { WorkoutCreateComponent } from './workout-create/workout-create.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { WorkoutListComponent } from './workout-list/workout-list.component';
import { JournalListComponent } from './journal-list/journal-list.component';
import { PostListComponent } from './post-list/post-list.component';
import { ResultsComponent } from './results/results.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  { path: '', redirectTo: 'profile', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'login', component: LoginComponent },
  { path: 'results', component: ResultsComponent },
  { path: 'users/:username', component: UsersComponent },
  { path: 'workout-create', component: WorkoutCreateComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    WorkoutCreateComponent,
    NavBarComponent,
    HomeComponent,
    ProfileComponent,
    LoginComponent,
    WorkoutListComponent,
    JournalListComponent,
    PostListComponent,
    ResultsComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

    RouterModule.forRoot(routes),

    ClarityModule,
    ClrFormsNextModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
