import { Component, OnInit, ViewChild } from '@angular/core';
import { timer } from 'rxjs';
import { IUser, IAccount } from '../lib/model';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountService } from '../services/account.service';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('f') formF;
  @ViewChild('g') formG;

  isOpen: boolean = false;

  account: IAccount = {
    name: '',
    email: '',
    password1: '',
    password2: '',
  }

  user: IUser = {
    email: '',
    password: '',
  }

  userMaster: IUser = {
    email: '',
    password: '',
  }

  creationSuccess: string = null;
  creationFailed: string = null;
  loginFailed: string = null;
  showPasswordWarning: boolean = false;


  constructor(private profileService: ProfileService, private accountService: AccountService, private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem("email") != null) {
    this.user.email = localStorage.getItem("email");
          this.profileService.getProfile(this.user).subscribe(
            profile => {
              this.profileService.userProfile$.next(profile);
              this.router.navigate(['/profile']);
            },
            err => {
            }
          );
        
    }
    this.accountService.loggedIn$.subscribe(
      val => {
      }
    );
  }

  login() {
    this.accountService.loggedIn$.next(true);
    this.user.email = this.userMaster.email;
    this.user.password = this.userMaster.password;
    this.accountService.login(this.user).subscribe(
      result => {
        let returnMessage = result
        if (result.success == true) {
          this.profileService.getProfile(this.user).subscribe(
            profile => {
              this.profileService.userProfile$.next(profile);
              this.router.navigate(['/profile']);
              localStorage.setItem("email", this.user.email);
            },
            err => {
            }
          );
        } else {
          this.loginFailed = 'Incorrect Username or Password';
        }
      },
      err => {
      }
    );
  }

  create() {
    if (this.formG.value.password1 != this.formG.value.password2) {
      this.creationFailed = 'Passwords Do Not Match';
    } else {
      this.accountService.createAccount(this.account).subscribe(
        result => {
          let returnMessage = result
          if (result.success == true) {
            this.creationSuccess = 'Account Created! Pleast Verify Account.';
            timer(2000).subscribe(
              val => {
                this.isOpen = false;
              }
            );
          } else {
            this.creationFailed = result.error + ' If you did not create the account, please email gymbuddy.2019@gmail.com';
          }
        },
        err => {
        }
      );
      this.formG.reset();
    }
  }

  onCancel() {
    this.isOpen = false;
    this.formG.reset();
    this.loginFailed = null;
    this.creationFailed = null;
    this.creationSuccess = null;
  }

  openModal() {
    this.onCancel();
    this.isOpen = true;
  }
}
