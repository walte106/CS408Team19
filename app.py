import os
import datetime
import ast

from flask import Flask, render_template, json, request, url_for, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail, Message
from itsdangerous import URLSafeTimedSerializer
from flask_cors import CORS, cross_origin

app = Flask(__name__)

CORS(app)

db_uri = "mysql+pymysql://"
db_uri += os.environ['DB_USERNAME'] + ":"
db_uri += os.environ['DB_PASSWORD']
db_uri += "@gymbuddy.cjh3gdb5wlv4.us-west-2.rds.amazonaws.com"
db_uri += "/GymBuddy"

app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_POOL_SIZE'] = 10

db = SQLAlchemy(app)

app.config["MAIL_SERVER"] = "smtp.googlemail.com"
app.config["MAIL_PORT"] = 465
app.config["MAIL_USE_TLS"] = False
app.config["MAIL_USE_SSL"] = True
app.config["MAIL_USERNAME"] = os.environ['APP_MAIL_USERNAME']
app.config["MAIL_PASSWORD"] = os.environ['APP_MAIL_PASSWORD']
app.config["MAIL_DEFAULT_SENDER"] = os.environ['APP_MAIL_USERNAME']
mail = Mail(app)

app.config["SECRET_KEY"] = os.environ['SECRET_KEY']
app.config["SECURITY_PASSWORD_SALT"] = os.environ['SECURITY_PASSWORD_SALT']

def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    return serializer.dumps(email, salt=app.config['SECURITY_PASSWORD_SALT'])

def confirm_token(token, expiration=86400):
    serializer = URLSafeTimedSerializer(app.config['SECRET_KEY'])
    try:
        email = serializer.loads(
                token,
                salt=app.config['SECURITY_PASSWORD_SALT'],
                max_age=expiration
        )
    except Exception as e:
        print(e)
        return False
    return email

@app.route("/")
def main():
    return render_template("exception.html")

@app.route('/confirm/<token>')
def confirm_email(token):
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()    
        _now = datetime.datetime.now()
        _email = confirm_token(token)
        if (_email == False):
            cursor.callproc('delete_user',(_email,_now))
            conn.commit()
            conn.close()
            return render_template("invalid.html")
        cursor.callproc('verify_user',(_email,_now))
        conn.commit()
        conn.close()
        return render_template("confirmed.html")
    except Exception as e:
        print(e)
        return render_template("exception.html")
        

@app.route("/signup",methods=['POST'])
def signup():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _email = request.json['email']
        temp = _email.split('@')
        _userName = temp[0]
        _name = request.json['name']
        _password = request.json['password']
        _registeredOn = datetime.datetime.now()
        cursor.callproc('create_user',(_email,_userName,_name,_password,_registeredOn))
        data = cursor.fetchall()
        if (len(data) == 0):
            conn.commit()
            conn.close()            
            token = generate_confirmation_token(_email)
            confirm_url = url_for('confirm_email',token = token, _external=True)
            template = render_template('activate.html',confirm_url=confirm_url)
            subject = "Verify email"
            msg = Message(
                subject,
                recipients=[_email],
                html=template,
                sender=app.config['MAIL_DEFAULT_SENDER']
            )
            mail.send(msg)
            return jsonify({'success':True,'message':'User created successfully! An email verification has been sent. Please verify within 1 day.'})
        else:
            conn.close()
            return jsonify({'success':False,'error':data[0][0]})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/login" , methods=['POST'])
def login():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _email = request.json['email']
        _password =request.json['password']
        cursor.callproc('login_user',(_email,_password))
        data = cursor.fetchall()
        conn.close()
        if (data[0][0] == "Login successful"):
            return jsonify({'success':True,'message':data[0][0]})
        else:
            return jsonify({'success':False,'error':data[0][0]})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/editBio" , methods=['PUT'])
def editBio():
    try:
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _name = request.json['name']
        _age = request.json['age']
        _height = request.json['height']
        _weight = request.json['weight']
        _bmi = request.json['bmi']
        _bio = request.json['bio']
        _status = request.json['status']
        if (_age == ""):
            _age = None
        if (_height == ""):
            _height = None
        if (_weight == ""):
            _weight = None
        if (_bmi == ""):
            _bmi = None
        if (_bio == ""):
            _bio = None
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        cursor.callproc('edit_bio',(_email,_name,_age,_height,_weight,_bmi,_bio,_status))
        conn.commit()
        conn.close()
        return jsonify({'message':'Edit Bio Successful'})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/addWorkout", methods=['PUT'])
def addWorkout():
    try:
        _date = request.json['date']
        _wName = request.json['wName']
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _url = request.json['url']
        _status = request.json['status']
        _exercises = request.json['exercises']
        _tags = request.json['tags']
        if (_url == ""):
            _url = None
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        cursor.callproc('add_workout',(_date,_wName,_email,_url,_status))
        data = cursor.fetchall()
        if (len(data) == 0):
            conn.commit()
            cursor.execute("select wId from Workout where (date = %s and wName = %s and email = %s);", (_date,_wName,_email))
            ID = cursor.fetchall()
            _wId = ID[0][0]
            for keys in _exercises:
                _type = keys
                _stats = _exercises[keys]
                cursor.callproc('add_exercise',(_wId,_type,_stats))
            for _tag in _tags:
                cursor.callproc('add_tag',(_wId,_tag))
            conn.commit()
            conn.close()
            return jsonify({'message':'Workout created successfully!'})
        else:
            conn.close()
            return jsonify({'error':data[0][0]})
    except Exception as e:
        print(e)
        return render_template("exception.html")
        

@app.route("/editWorkout", methods=['PUT'])
def editWorkout():
    try:
        _date = request.json['date']
        _wName = request.json['wName']
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _url = request.json['url']
        _status = request.json['status']
        _exercises = request.json['exercises']
        _tags = request.json['tags']
        if (_url == ""):
            _url = None
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        cursor.execute("select wId from Workout where (date = %s and wName = %s and email = %s);", (_date,_wName,_email))
        ID = cursor.fetchall()
        if (len(ID) == 0):
            conn.close()
            return jsonify({'error':'Workout doesn\'t exist'})
        else:
            _wId = ID[0][0]
            cursor.callproc('edit_workout',(_wId,_url,_status))
            for keys in _exercises:
                _type = keys
                _stats = _exercises[keys]
                cursor.callproc('add_exercise',(_wId,_type,_stats))
            for _tag in _tags:
                cursor.callproc('add_tag',(_wId,_tag))
            conn.commit()
            conn.close()
            return jsonify({'message':'Workout edited successfully!'})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/deleteWorkout", methods=['DELETE'])
def deleteWorkout():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _date = request.json['date']
        _wName = request.json['wName']
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        cursor.execute("select wId from Workout where (date = %s and wName = %s and email = %s);", (_date,_wName,_email))
        ID = cursor.fetchall()
        if (len(ID) == 0):
            conn.close()
            return jsonify({'error':'Workout doesn\'t exist'})
        else:
            _wId = ID[0]
            cursor.callproc('delete_workout',(_wId))
            conn.commit()
            conn.close()
            return jsonify({'message':'Workout deleted successfully!'})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/addDiet", methods=['PUT'])
def addDiet():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _date = request.json['date']
        _dName = request.json['dName']
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _entry = request.json['entry']
        _status = request.json['status']
        cursor.callproc('add_diet',(_date,_dName,_email,_entry,_status))
        data = cursor.fetchall()
        if (len(data) == 0):
            conn.commit()
            conn.close()
            return jsonify({'message':'Diet created successfully!'})
        else:
            conn.close()
            return jsonify({'error':data[0][0]})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/editDiet", methods=['PUT'])
def editDiet():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _date = request.json['date']
        _dName = request.json['dName']
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _entry = request.json['entry']
        _status = request.json['status']
        cursor.execute("select dId from Diet where (date = %s and dName = %s and email = %s);", (_date,_dName,_email))
        ID = cursor.fetchall()
        if (len(ID) == 0):
            conn.close()
            return jsonify({'error':'Diet doesn\'t exist'})
        else:
            _dId = ID[0]
            cursor.callproc('edit_diet',(_dId,_entry,_status))
            conn.commit()
            conn.close()
            return jsonify({'message':'Diet edited successfully!'})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/deleteDiet", methods=['DELETE'])
def deleteDiet():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _date = request.json['date']
        _dName = request.json['dName']
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        cursor.execute("select dId from Diet where (date = %s and dName = %s and email = %s);", (_date,_dName,_email))
        ID = cursor.fetchall()
        if (len(ID) == 0):
            conn.close()
            return jsonify({'error':'Diet doesn\'t exist'})
        else:
            _dId = ID[0]
            cursor.callproc('delete_diet',(_dId))
            conn.commit()
            conn.close()
            return jsonify({'message':'Diet deleted successfully!'})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/searchByUser", methods=['POST'])
def searchByUser():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _searchQuery = request.json['searchQuery']
        if (_searchQuery == ""):
            conn.close()
            return jsonify([])
        _searchQuery+="%"
        cursor.execute("select userName,name from User where (verified = 1 and email != %s and userName like %s) ORDER BY userName ASC;",(_email,_searchQuery))
        data = cursor.fetchall()
        if (len(data) == 0):
            conn.close()
            return jsonify({'message':'User not found'})
        allUsers = []
        for u in data:
            user = {}
            user['userName'] = str(u[0])
            user['name'] = str(u[1])
            allUsers.append(user)
        conn.close()
        return jsonify(allUsers)
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/searchByTag", methods=['POST'])
def searchByTag():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _tag = request.json['tag']
        if (_tag == []):
            conn.close()
            return jsonify([])
        format_strings = ','.join(['%s']*len(_tag))
        cursor.execute("select wId from Tags where tag in (%s);"%format_strings,tuple(_tag))
        data = cursor.fetchall()
        if (len(data) == 0):
            conn.close()
            return jsonify({'message':'Tags not found'})
        wIds = {}
        for t in data:
            if (t[0] not in wIds):
                wIds[t[0]] = 1
            else:
                wIds[t[0]] += 1
        allWorkouts = []
        for i in sorted(wIds.items(), key=lambda kv: kv[1], reverse=True):
            cursor.execute("select email,wName from Workout where (status = 2 and wId = %s and email != %s);",(i[0],_email))
            data = cursor.fetchall()
            if (len(data) != 0):
                workout = {}
                user = str(data[0][0])
                workout['userName'] = user[0:user.find('@')]
                workout['wName'] = str(data[0][1])
                allWorkouts.append(workout)
        conn.close()
        return jsonify(allWorkouts)
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/isFriend", methods=['GET'])
def isFriend():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.args.get('userName')
        _email = _userName + "@purdue.edu"
        _friendUserName = request.args.get('friendUserName')
        _friendEmail = _friendUserName + "@purdue.edu"
        cursor.execute("select 1 from Friends where (email = %s and friendEmail= %s and status = 1);",(_email,_friendEmail))
        data=cursor.fetchall()
        conn.close()
        if (len(data) != 0):
            return jsonify({'status':True,'message':'IS FRIEND'})
        return jsonify({'status':False,'message':'IS NOT FRIEND'})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/addPost", methods=['PUT'])
def addPost():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _wDate = request.json['wDate']
        _wName = request.json['wName']
        _dDate = request.json['dDate']
        _dName = request.json['dName']
        _entry = request.json['entry']
        _pName = request.json['pName']
        _pDate = request.json['pDate']
        _status = request.json['status']
        _wId = None
        _dId = None
        if (_wName != ""):
            cursor.execute("select wId from Workout where (date = %s and wName = %s and email = %s and status >= %s);", (_wDate,_wName,_email,_status))
            wId = cursor.fetchall()
            if(len(wId)==0):
                conn.close()
                return jsonify({'error':'Please select valid workout'})
            _wId = wId[0]
        if (_dName != ""):
            cursor.execute("select dId from Diet where (date = %s and dName = %s and email = %s and status >= %s);", (_dDate,_dName,_email,_status))
            dId = cursor.fetchall()
            if(len(dId)==0):
                conn.close()
                return jsonify({'error':'Please select valid diet'})
            _dId = dId[0]
        cursor.callproc('add_post',(_pDate,_pName,_email,_wId,_dId,_entry,_status))
        data=cursor.fetchall()
        if(len(data) == 0):
            conn.commit()
            conn.close()
            return jsonify({'message':'Post created successfully!'})
        else:
            conn.close()
            return jsonify({'error':data[0][0]})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/editPost", methods=['PUT'])
def editPost():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        _wDate = request.json['wDate']
        _wName = request.json['wName']
        _dDate = request.json['dDate']
        _dName = request.json['dName']
        _entry = request.json['entry']
        _pName = request.json['pName']
        _pDate = request.json['pDate']
        _status = request.json['status']
        _wId = None
        _dId = None
        if (_wName != ""):
            cursor.execute("select wId from Workout where (date = %s and wName = %s and email = %s and status >= %s);", (_wDate,_wName,_email,_status))
            wId = cursor.fetchall()
            if(len(wId)==0):
                conn.close()
                return jsonify({'error':'Please select valid workout'})
            _wId = wId[0]
        if (_dName != ""):
            cursor.execute("select dId from Diet where (date = %s and dName = %s and email = %s and status >= %s);", (_dDate,_dName,_email,_status))
            dId = cursor.fetchall()
            if(len(dId)==0):
                conn.close()
                return jsonify({'error':'Please select valid diet'})
            _dId = dId[0]
        cursor.execute("select pId from Post where (date = %s and pName = %s and email = %s);", (_pDate,_pName,_email))
        pId = cursor.fetchall()
        if(len(pId) == 0):
            conn.close()
            return jsonify({'error':'Post doesn\'t exist'})
        else:
            _pId = pId[0]
            cursor.callproc('edit_post',(_pId,_wId,_dId,_entry,_status))
            conn.commit()
            conn.close()
            return jsonify({'message':'Post edited successfully!'})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/deletePost", methods=['DELETE'])
def deletePost():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _date = request.json['date']
        _pName = request.json['pName']
        _userName = request.json['userName']
        _email = _userName + "@purdue.edu"
        cursor.execute("select pId from Post where (date = %s and pName = %s and email = %s);", (_date,_pName,_email))
        ID = cursor.fetchall()
        if(len(ID) == 0):
            conn.close()
            return jsonify({'error':'Post doesn\'t exist'})
        else:
            _pId = ID[0]
            cursor.callproc('delete_post',(_pId))
            conn.commit()
            conn.close()
            return jsonify({'message':'Post deleted successfully!'})
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/addFriend", methods=['POST'])
def addFriend():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.json['userName']
        _friendUserName = request.json['friendUserName']
        _email = _userName + "@purdue.edu"
        _friendEmail = _friendUserName + "@purdue.edu"
        if (_email == _friendEmail):
            return jsonify({'message':'Cannot add yourself as friend'})
        cursor.callproc('add_friend',(_email,_friendEmail))
        data = cursor.fetchall()
        conn.commit()
        conn.close()
        if(len(data)==0):
            return jsonify({'message':'Friend request has been sent'})
        return jsonify(data[0][0])
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/acceptFriend", methods=['POST'])
def acceptFriend():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.json['userName']
        _friendUserName = request.json['friendUserName']
        _email = _userName + "@purdue.edu"
        _friendEmail = _friendUserName + "@purdue.edu"
        cursor.callproc('accept_friend',(_email,_friendEmail))
        data=cursor.fetchall()
        conn.commit()
        conn.close()
        if(len(data)==0):
            return jsonify({'message':'Friend has been accepted'})
        return jsonify(data[0][0])
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/rejectFriend", methods=['POST'])
def rejectFriend():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.json['userName']
        _friendUserName = request.json['friendUserName']
        _email = _userName + "@purdue.edu"
        _friendEmail = _friendUserName + "@purdue.edu"
        cursor.callproc('delete_friend',(_email,_friendEmail))
        data=cursor.fetchall()
        conn.commit()
        conn.close()
        if (len(data) == 0):
            return jsonify({'message':'Friend has been rejected'})
        return jsonify(data[0][0])
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/pendingFriendRequest", methods=['GET'])
def pendingFriendRequest():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.args.get('userName')
        _email = str(_userName) + "@purdue.edu"
        cursor.execute("SELECT userName from User where (email in (SELECT email from Friends WHERE status = 0 and friendEmail= %s)) ;",(_email))
        data=cursor.fetchall()
        conn.close()
        friends = []
        if (len(data) != 0):
            for f in data:
                friends.append(str(f[0]))
        return jsonify(friends)
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/viewAllFriends", methods=['GET'])
def viewAllFriends():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.args.get('userName')
        _email = str(_userName) + "@purdue.edu"
        cursor.execute("SELECT friendEmail from Friends WHERE email = %s ;",(_email))
        data = cursor.fetchall()
        conn.commit()
        if (len(data) == 0):
            return jsonify({'message':'LONER ALERT'})
        return jsonify(data)
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/viewAllBios", methods=['GET'])
def viewAllBios():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.args.get('userName')
        _email = str(_userName) + "@purdue.edu"
        _status = 3
        _callType= request.args.get('callType')
        if (_callType == "private"):
            _status = 0
        elif (_callType == "friend"):
            _status = 1
        elif (_callType == "public"):
            _status = 2
        cursor.execute("SELECT userName,name,age,height,weight,bmi,bio,status from User WHERE (email = %s and status >= %s) ;",(_email,_status))
        data = cursor.fetchall()
        if (len(data) == 0):
            conn.close()
            return jsonify({'message':'No bio'})
        bio = {}
        bio['userName'] = data[0][0]
        bio['name'] = data[0][1]
        bio['age'] = data[0][2]
        bio['height'] = data[0][3]
        bio['weight'] = data[0][4]
        bio['bmi'] = data[0][5]
        bio['bio'] = data[0][6]
        bio['status'] = data[0][7]
        conn.close()
        return jsonify(bio)
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/viewAllWorkouts", methods=['GET'])
def viewAllWorkouts():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.args.get('userName')
        _email = str(_userName) + "@purdue.edu"
        _status = 3
        _callType = request.args.get('callType')
        if (_callType == "private"):
            _status = 0
        elif (_callType == "friend"):
            _status = 1
        elif (_callType == "public"):
            _status = 2
        cursor.execute("select wId from Workout where (email = %s and status >= %s);", (_email,_status))
        data = cursor.fetchall()
        allWorkouts = []
        if (len(data) != 0):
            for wId in data:
                workout = {}
                exercise = {}
                tag = []
                cursor.execute("select wName,date,email,playlistURL,status from Workout where wId = %s;",(wId[0]))
                wInfo = cursor.fetchall()
                workout['wName'] = str(wInfo[0][0])
                workout['date'] = str(wInfo[0][1])
                workout['email'] = str(wInfo[0][2])
                if (wInfo[0][3] == None):
                    workout['url'] = None
                else:
                    workout['url'] = str(wInfo[0][3])
                workout['status'] = int(wInfo[0][4])
                cursor.execute("select type,stats from Exercise where wId = %s;",(wId[0]))
                eInfo = cursor.fetchall()
                for e in eInfo:
                    exercise[str(e[0])] = str(e[1])
                workout['exercises'] = exercise
                cursor.execute("select tag from Tags where wId = %s;",(wId[0]))
                tInfo = cursor.fetchall()
                for t in tInfo:
                    tag.append(str(t[0]))
                workout['tags'] = tag
                allWorkouts.append(workout)
        conn.close()
        return jsonify(allWorkouts)
    except Exception as e:
        return render_template("exception.html")

@app.route("/viewAllDiets", methods=['GET'])
def viewAllDiets():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.args.get('userName')
        _email = str(_userName) + "@purdue.edu"
        _status = 3
        _callType= request.args.get('callType')
        if (_callType == "private"):
            _status = 0
        elif (_callType == "friend"):
            _status = 1
        elif (_callType == "public"):
            _status = 2
        cursor.execute("select dId from Diet where (email = %s and status >= %s);", (_email,_status))
        data = cursor.fetchall()
        allDiets = []
        if (len(data) != 0):
            for dId in data:
                diet = {}
                cursor.execute("select dName,date,email,entry,status from Diet where dId = %s;",(dId[0]))
                dInfo = cursor.fetchall()
                diet['dName'] = str(dInfo[0][0])
                diet['date'] = str(dInfo[0][1])
                diet['email'] = str(dInfo[0][2])
                diet['entry'] = str(dInfo[0][3])
                diet['status'] = int(dInfo[0][4])
                allDiets.append(diet)
        conn.close()
        return jsonify(allDiets)
    except Exception as e:
        print(e)
        return render_template("exception.html")

@app.route("/viewAllPosts", methods=['GET'])
def viewAllPosts():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.args.get('userName')
        _email = str(_userName) + "@purdue.edu"
        _status = 3
        _callType= request.args.get('callType')
        if (_callType == "private"):
            _status = 0
        elif (_callType == "friend"):
            _status = 1
        elif (_callType == "public"):
            _status = 2
        cursor.execute("select pId from Post where (email = %s and status >= %s);", (_email,_status))
        data = cursor.fetchall()
        allPosts = []
        if (len(data) != 0):
            for pId in data:
                post = {}
                workout = {}
                exercise = {}
                diet = {}
                tag = []
                cursor.execute("select pName,date,email,wId,dId,entry,status from Post where pId = %s;",(pId[0]))
                pInfo = cursor.fetchall()
                post['pName'] = str(pInfo[0][0])
                post['date'] = str(pInfo[0][1])
                post['email'] = str(pInfo[0][2])
                post['entry'] = str(pInfo[0][5])
                post['status'] = int(pInfo[0][6])
                if (pInfo[0][3] != None):
                    wId = pInfo[0][3]
                    cursor.execute("select wName,date,email,playlistURL,status from Workout where wId = %s;",(wId))
                    wInfo = cursor.fetchall()
                    workout['wName'] = str(wInfo[0][0])
                    workout['date'] = str(wInfo[0][1])
                    workout['email'] = str(wInfo[0][2])
                    if (wInfo[0][3] == None):
                        workout['url'] = None
                    else:
                        workout['url'] = str(wInfo[0][3])
                    workout['status'] = int(wInfo[0][4])
                    cursor.execute("select type,stats from Exercise where wId = %s;",(wId))
                    eInfo = cursor.fetchall()
                    for e in eInfo:
                        exercise[str(e[0])] = str(e[1])
                    workout['exercises'] = exercise
                    cursor.execute("select tag from Tags where wId = %s;",(wId))
                    tInfo = cursor.fetchall()
                    for t in tInfo:
                        tag.append(str(t[0]))
                    workout['tags'] = tag
                if (workout != {}):
                    post['workout'] = [workout]
                else:
                    post['workout'] = []
                if (pInfo[0][4] != None):
                    dId = pInfo[0][4]
                    cursor.execute("select dName,date,email,entry,status from Diet where dId = %s;",(dId))
                    dInfo = cursor.fetchall()
                    diet['dName'] = str(dInfo[0][0])
                    diet['date'] = str(dInfo[0][1])
                    diet['email'] = str(dInfo[0][2])
                    diet['entry'] = str(dInfo[0][3])
                    diet['status'] = int(dInfo[0][4])
                if (diet != {}):
                    post['diet'] = [diet]
                else:
                    post['diet'] = []
                allPosts.append(post)
        conn.close()
        return jsonify(allPosts)
    except Exception as e:
        return render_template("exception.html")

@app.route("/viewFriendsPosts", methods=['GET'])
def viewFriendsPosts():
    try:
        conn = db.engine.raw_connection()
        cursor = conn.cursor()
        _userName = request.args.get('userName')
        _email = str(_userName) + "@purdue.edu"
        posts = {}
        cursor.execute("select pId,date from Post where (status >= 1 and email in (SELECT friendEmail from Friends WHERE status = 1 and email = %s));", (_email))
        data = cursor.fetchall()
        for p in data:
            posts[p[0]] = str(p[1])
        cursor.execute("select pId,date from Post where email = %s ;", (_email))
        data = cursor.fetchall()
        for p in data:
            posts[p[0]] = str(p[1])
        allPosts = []
        if (len(data) != 0):
            for pId in sorted(posts,key = posts.get,reverse=True):
                post = {}
                workout = {}
                exercise = {}
                diet = {}
                tag = []
                cursor.execute("select pName,date,email,wId,dId,entry,status from Post where pId = %s;",(pId))
                pInfo = cursor.fetchall()
                post['pName'] = str(pInfo[0][0])
                post['date'] = str(pInfo[0][1])
                post['email'] = str(pInfo[0][2])
                post['entry'] = str(pInfo[0][5])
                post['status'] = int(pInfo[0][6])
                if (pInfo[0][3] != None):
                    wId = pInfo[0][3]
                    cursor.execute("select wName,date,email,playlistURL,status from Workout where wId = %s;",(wId))
                    wInfo = cursor.fetchall()
                    workout['wName'] = str(wInfo[0][0])
                    workout['date'] = str(wInfo[0][1])
                    workout['email'] = str(wInfo[0][2])
                    if (wInfo[0][3] == None):
                        workout['url'] = None
                    else:
                        workout['url'] = str(wInfo[0][3])
                    workout['status'] = int(wInfo[0][4])
                    cursor.execute("select type,stats from Exercise where wId = %s;",(wId))
                    eInfo = cursor.fetchall()
                    for e in eInfo:
                        exercise[str(e[0])] = str(e[1])
                    workout['exercises'] = exercise
                    cursor.execute("select tag from Tags where wId = %s;",(wId))
                    tInfo = cursor.fetchall()
                    for t in tInfo:
                        tag.append(str(t[0]))
                    workout['tags'] = tag
                if (workout != {}):
                    post['workout'] = [workout]
                else:
                    post['workout'] = []
                if (pInfo[0][4] != None):
                    dId = pInfo[0][4]
                    cursor.execute("select dName,date,email,entry,status from Diet where dId = %s;",(dId))
                    dInfo = cursor.fetchall()
                    diet['dName'] = str(dInfo[0][0])
                    diet['date'] = str(dInfo[0][1])
                    diet['email'] = str(dInfo[0][2])
                    diet['entry'] = str(dInfo[0][3])
                    diet['status'] = int(dInfo[0][4])
                if (diet != {}):
                    post['diet'] = [diet]
                else:
                    post['diet'] = []
                allPosts.append(post)
        conn.close()
        return jsonify(allPosts)
    except Exception as e:
        return render_template("exception.html")

if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=True,threaded=True,use_reloader=True)
