CREATE DEFINER=`gymbuddyback`@`%` PROCEDURE `add_diet`(
	IN in_date DATE,
    IN in_dName VARCHAR(45),
    IN in_email VARCHAR(255),
    IN in_entry VARCHAR(300),
    IN in_status INT
)
BEGIN
	DECLARE EXIT HANDLER FOR 1452 SELECT 'User doesn\'t exit.';

	if (select exists (select 1 from Diet where date = in_date and dName = in_dName and email = in_email)) then
		select 'User Exists!';
	
    else
		insert into Diet (
			date,
            dName,
            email,
            entry,
            status
		)
		values (
			in_date,
            in_dName,
			in_email,
            in_entry,
            in_status
        );
	END if;
END